use vector_space::{interpolate, VectorSpace};

pub struct View<V: VectorSpace> {
    pub zoom: V::Scalar,
    pub offset: V,
}

impl<V: VectorSpace> View<V> {
    pub fn zoom(&mut self, factor: V::Scalar, center: V) {
        self.zoom = self.zoom * factor;
        self.offset = interpolate(center, self.offset, factor);
    }

    pub fn clip(&self, pos: V) -> V {
        pos * self.zoom + self.offset
    }

    pub fn unclip(&self, pos: V) -> V {
        (pos - self.offset) / self.zoom
    }
}
